﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sintaxis
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("TEST");
            string asd = "123";
            int b = Int32.Parse("10");
            int qwe = int.Parse("10");
            Boolean x = true;
            qwe = qwe + b;

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            int[] arreglo = { 1, 2, 3, 4, 5, 6, 7, 8 };
            foreach (int variable in arreglo)
            {
                if (variable % 2 == 0) { continue; }
                Console.Write(variable + " ");

            }

            while (b > 0)
            {
                b--;
                if (b == 6 || b == 5)
                {
                    continue;
                }

                int d = b + qwe;
                Console.WriteLine("El valor es {0} + {1} = {2} ", b, qwe, d);

            }




            ETIQUETA_UNO:

            Console.WriteLine("Medidas: 1. Pequeño 2. Mediano 3. Grande");
            Console.WriteLine("Seleccione una opción: ");
            int costo = 0;
            string opcion = Console.ReadLine();
            int op = Int32.Parse(opcion);

            switch (op)
            {
                case 1:
                    costo += 25;
                    break;
                case 2:
                    costo += 25;
                    goto case 1;
                case 3:
                    costo += 50;
                    goto case 1;
                default:
                    Console.WriteLine("Selección Inválida");
                    break;
            }
            Console.WriteLine("El costo del café es: {0:0.00}", costo);

            Console.WriteLine("Desea volver a iniciar: S/N");
            opcion = Console.ReadLine();
            if (opcion.Equals("S"))
            {
                goto ETIQUETA_UNO;
            }


            Animal anim = new Animal()
            {
                Id = 1,
                Nombre = "Capitan",
                Raza = "Pastor",
                Sexo = 'M'
            };

            anim.Nombre = "Tobby";
            Console.WriteLine(anim.Nombre);

            int[] arregloDos = { 1, 2, 3 };
            int[,] matriz = new int[3, 4];
            matriz[0, 0] = 1;

            for (int f = 0; f < matriz.GetLength(0); f++)
            {
                for (int c = 0; c < matriz.GetLength(1); c++)
                {
                    Console.Write(matriz[f,c] + " ");
                }
                Console.WriteLine(  );
            }
            

            Console.WriteLine("\nPresione una tecla para continuar.....");
            Console.ReadKey();

        }


    }
}
