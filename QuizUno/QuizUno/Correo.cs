﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizUno
{
    class Correo
    {
        public double CostoEnvio(int gramos)
        {
            double pago = 0;
            if (gramos > 200)
            {
                return 0;
            }

            if (gramos > 30)
            {
                pago = (gramos - 30) * 1.5 + 20 + 10;
            }
            else if (gramos > 20)
            {
                pago = (gramos - 20) * 2 + 10;
            }
            else
            {
                pago = 10;
            }

            return pago;
        }
    }
}
