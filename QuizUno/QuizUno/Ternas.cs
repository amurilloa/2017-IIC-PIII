﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizUno
{
    class Ternas
    {
        public string BuscarTernas()
        {
            string mensaje = "";
            for (int a = 0; a < 25; a++)
            {
                for (int b = a + 1; b < 25; b++)
                {
                    for (int c = b + 1; c < 36; c++)
                    {
                        if ((Math.Pow(a, 2) + Math.Pow(b, 2)) == Math.Pow(c, 2))
                        {
                            mensaje += a + "," + b + "," + c + "\n";
                        }

                    }
                }
            }
            return mensaje;
        }
    }
}
