﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizUno
{
    class Luz
    {
        public double PagoLuz(int consumidos)
        {
            double pago = 0;
            if (consumidos < 12)
            {
                pago = 10;
            }
            else if (consumidos >= 12 && consumidos <= 65)
            {
                pago = 10 + ((consumidos - 11) * 2);
            }
            else
            {
                pago = 10 + 108 + ((consumidos - 65) * 4);
            }

            return pago;
        }
    }
}
