﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizUno
{
    class Program
    {
        static void Main(string[] args)
        {
            Luz l = new Luz();
            Console.WriteLine(l.PagoLuz(11));
            Console.WriteLine(l.PagoLuz(12));
            Console.WriteLine(l.PagoLuz(13));
            Console.WriteLine(l.PagoLuz(65));
            Console.WriteLine(l.PagoLuz(66));

            Ternas t = new Ternas();
            Console.WriteLine(t.BuscarTernas());

            Correo c = new Correo();
            Console.WriteLine(c.CostoEnvio(19));
            Console.WriteLine(c.CostoEnvio(20));
            Console.WriteLine(c.CostoEnvio(21));
            Console.WriteLine(c.CostoEnvio(30));
            Console.WriteLine(c.CostoEnvio(31));
            Console.WriteLine(c.CostoEnvio(201));

            Console.ReadKey();
        }
    }
}
