﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticasSemanaUnoDos
{
    class Program
    {
        static void Main(string[] args)
        {
            string menu = @"
Menú: 
    1. Sumar. 
    2. Restar. 
    3. Multiplicación. 
    4. División. 
    5. Llenar un arreglo. 
    6. Imprimir el arreglo 
    7. Salir
Seleccione una opción:";
            Operaciones ope = new Operaciones();
            MENU:
            Console.Clear();
            Console.WriteLine(menu);
            int op = Int32.Parse(Console.ReadLine());
            int num1 = 0;
            int num2 = 0;
            if (op >= 1 && op <= 4)
            {
                Console.WriteLine("Digite el número 1");
                num1 = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Digite el número 2");
                num2 = Int32.Parse(Console.ReadLine());
            }

            switch (op)
            {
                case 1:
                    Console.WriteLine("El resutaldo de {0} + {1} = {2}", num1, num2, ope.Sumar(num1, num2));
                    Console.ReadKey();
                    break;
                case 2:
                    Console.WriteLine("El resutaldo de {0} - {1} = {2}", num1, num2, ope.Restar(num1, num2));
                    Console.ReadKey();
                    break;
                case 3:
                    Console.WriteLine("El resutaldo de {0} * {1} = {2}", num1, num2, ope.Multiplicar(num1, num2));
                    Console.ReadKey();
                    break;
                case 4:
                    Console.WriteLine("El resutaldo de {0} / {1} = {2}", num1, num2, ope.Division(num1, num2));
                    Console.ReadKey();
                    break;
                case 5:
                    Console.WriteLine("Digite la cantidad de elementos");
                    num1 = Int32.Parse(Console.ReadLine());
                    ope.CrearArreglo(num1);
                    goto case 6;
                case 6:
                    Console.WriteLine(ope.ImprimirArreglo());
                    Console.ReadKey();
                    break;
                case 7:
                    goto SALIR;
            }
            goto MENU;

            SALIR:
            Console.ReadKey();


        }
    }
}
