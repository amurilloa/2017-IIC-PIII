﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticasSemanaUnoDos
{
    class Operaciones
    {

        private int[] arreglo;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cantidad"></param>
        public void CrearArreglo(int cantidad)
        {
            arreglo = new int[cantidad];
            Random r = new Random();
            for (int i = 0; i < arreglo.Length; i++)
            {
                arreglo[i] = r.Next(cantidad);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ImprimirArreglo()
        {
            string texto = "";
            int con = 0; //Cuando se ocupa el contador es mejor un for "normal"
            foreach (int item in arreglo)
            {
                texto += "[" + con + "]=" + item + "\n";
                con++;
            }

            return texto;
        }


        /// <summary>
        /// Resta dos elementos enteros
        /// </summary>
        /// <param name="num1">Número 1</param>
        /// <param name="num2">Número 2</param>
        /// <returns>El resultado de la resta de Numero 1 - Número 2</returns>
        public int Restar(int num1, int num2)
        {
            return num1 - num2;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        public int Sumar(int num1, int num2)
        {

            return num1 + num2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        public int Multiplicar(int num1, int num2)
        {

            return num1 + num2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns></returns>
        public double Division(int num1, int num2)
        {
            if (num2 != 0)
            {
                return num1 + num2;
            }
            else {
                return 0;
            }
        }
    }
}
