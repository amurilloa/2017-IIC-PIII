﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Mesa
    {
        public int NumeroMesa { get; set; }
        public string[] MENU = {"Hamburguesa Sencilla",
            "Hamburguesa con Queso",
            "Hamburguesa Especial",
            "Papas Fritas",
            "Refresco",
            "Postre" };
        public double[] Precios { get; set; }
        public double[] Pedido { get; set; }

        public Mesa(int numeroMesa, double[] precios)
        {
            NumeroMesa = numeroMesa;
            Pedido = new double[MENU.Length];
            Precios = precios;
        }

        public double CalcularTotal()
        {
            double total = 0;
            for (int i = 0; i < Pedido.Length; i++)
            {
                total += Pedido[i] * Precios[i];
            }

            return total;
        }
    }
}
