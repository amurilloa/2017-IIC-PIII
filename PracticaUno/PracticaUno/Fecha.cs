﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Fecha
    {
        private int dia;
        private int mes;
        private int anno;

        public Fecha()
        {
            this.dia = 1;
            this.mes = 1;
            this.anno = 1800;
        }


        public Fecha(int dia, int mes, int anno)
        {
            if (VerificarFecha(dia, mes, anno))
            {
                this.dia = dia;
                this.mes = mes;
                this.anno = anno;
            }
            else
            {
                this.dia = 1;
                this.mes = 1;
                this.anno = 1800;
            }
        }

        private bool VerificarFecha(int dia, int mes, int anno)
        {
            //Minimo que debería cumplir
            if (dia < 1 || mes < 1 || dia > 31 || mes > 12)
            {
                return false;
            }

            //Febrero
            if (mes == 2)
            {
                if (dia >= 30)
                {
                    return false;
                }
                if (!EsBisiesto(anno) && dia >=29) {
                    return false;
                }
            }
            
            if((mes ==4|| mes == 6 || mes == 10 || mes == 11) && dia >= 31)
            {
                return false;
            }

            return true;

        }

        private bool EsBisiesto(int anno)
        {
            if (anno % 4 == 0)
            {
                if (anno % 100 == 0)
                {
                    if (anno % 400 == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        public string FechaCorta()
        {
            return dia.ToString("00") + "/" + mes.ToString("00") +"/" + anno.ToString("00");
        }

        public string FechaLarga()
        {
            return dia.ToString("00") + " de " + MesLetras(mes) + " de " + anno.ToString("00");
        }

        private String MesLetras(int mes)
        {
            switch (mes)
            {
                case 1:
                    return "Enero";
                case 2:
                    return "Febrero";
                case 3:
                    return "Marzo";
                case 4:
                    return "Abril";
                case 5:
                    return "Mayo";
                case 6:
                    return "Junio";
                case 7:
                    return "Julio";
                case 8:
                    return "Agosto";
                case 9:
                    return "Septiembre";
                case 10:
                    return "Octubre";
                case 11:
                    return "Noviembre";
                case 12:
                    return "Diciembre";
                default:
                    return "";
            }
        }
    }
}
