﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Articulo
    {
        public int Clave { get; set; }
        public string Descripcion { get; set; }
        public double Precio { get; set; }
        public int Cantidad { get; set; }


        public double CalcularIVA()
        {
            return Precio * 0.13;
        }

    }
}
