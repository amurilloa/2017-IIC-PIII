﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Program
    {
        static double CostoAuto(double monto)
        {
            return monto * (1 + 0.06 + 0.12);
        }

        static double DiasSegundos(double dias)
        {
            return dias * 24 * 60 * 60;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensaje"></param>
        /// <returns></returns>
        static int LeerInt(string mensaje)
        {
            ET:
            Console.WriteLine(mensaje);
            string texto = Console.ReadLine();
            int num = 0;
            if (!Int32.TryParse(texto, out num))
            {
                Console.Write("Favor intente nuevamente, ");
                goto ET;
            }
            return num;
        }

        static void Main(string[] args)
        {
            Circunferencia rueda = new Circunferencia { Radio = 10.2 };
            Circunferencia moneda = new Circunferencia { Radio = 1.4 };

            Console.WriteLine("El área de la rueda es {0:00}cm²", rueda.CalcularArea());
            Console.WriteLine("El área de la moneda es {0:00}cm²", moneda.CalcularArea());
            Console.WriteLine("El perímetro de la rueda es {0:00}", rueda.CalcularPerimetro());
            Console.WriteLine("El perímetro de la moneda es {0:00}", moneda.CalcularPerimetro());

            Rectangulo pared = new Rectangulo { Largo = 10, Ancho = 10 };
            Rectangulo ventana = new Rectangulo { Largo = 3, Ancho = 2 };
            double total = pared.CalcularArea() - ventana.CalcularArea();
            double tiempo = total * 10;
            int hor = (int)tiempo / 60;
            int min = (int)tiempo % 60;
            Console.WriteLine("Total de área a pintar {0:00}m², tiempo aproximado {1:00}hrs {2:00}min", total, hor, min);

            Fecha fechaUno = new Fecha();
            Fecha fechaDos = new Fecha(31, 2, 1232);
            Fecha fechaTres = new Fecha(29, 2, 2000);
            Console.WriteLine(fechaUno.FechaCorta());
            Console.WriteLine(fechaDos.FechaCorta());
            Console.WriteLine(fechaTres.FechaLarga());

            Articulo articulo = new Articulo
            {
                Clave = 1,
                Descripcion = "Arroz",
                Precio = 1300,
                Cantidad = 92
            };

            Console.WriteLine("El IVA del {0} es de {1:00}", articulo.Descripcion, articulo.CalcularIVA());

            Temperatura temp = new Temperatura { GradosCentigrados = 30 };
            Console.WriteLine("Temperatura {0}C\nTemperatura {1}F", temp.GradosCentigrados, temp.ConvertirAFarenheit());

            double colones = 57351;
            CambioDivisas cd = new CambioDivisas(573.51) { Colones = colones };
            Console.WriteLine("{0} colones son {1:0.00} dolares", colones, cd.ConvertirADolares());

            double galones = 5;
            Gasolinera gas = new Gasolinera(597);
            Console.WriteLine("El costo por {0} galones de gasolina es {1} colones", galones, gas.CobroEnLitros(galones));


            double[] precios = { 15, 18, 20, 8, 5, 6 };
            Mesa[] mesas = new Mesa[5];
            for (int i = 0; i < mesas.Length; i++)
            {
                mesas[i] = new Mesa(i + 1, precios);
            }

            string menu = "1. Capturar Orden" +
                "\n2. Calcular Cuenta" +
                "\n3.Salir" +
                "\nSelecione una opción: ";

            while (true)
            {
                int opcion = LeerInt(menu);
                if (opcion == 3)
                {
                    break;
                }
                int mesa = LeerInt("Digite el número de mesa 1-5");
                if (opcion == 1)
                {
                    for (int i = 0; i < mesas[mesa].Pedido.Length; i++)
                    {
                        mesas[mesa].Pedido[i] = LeerInt("Cuantas " + mesas[mesa].MENU[i] + "desea: ");
                    }
                }

                if (opcion == 2)
                {
                    Console.WriteLine("El costo de su pedido es {0} dolares", mesas[mesa].CalcularTotal());
                }

            }

            double monto = 1000000;
            Console.WriteLine("El costo del vehículo para un monto de {0:.00} es de {1:.00}", monto, CostoAuto(monto));

            Console.WriteLine("{0} dias son {1} segundos", 5, DiasSegundos(5));

            Telefono tel = new Telefono { Duracion = 4 };


            Console.WriteLine("El costo de una llamada de 4 minutos es de ${0}",tel.CalcularCosto());
            Console.ReadKey();
        }
    }
}
