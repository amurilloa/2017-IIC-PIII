﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class CambioDivisas
    {
        public double Colones { get; set; }
        public double TipoCambio { get; set; }

        public CambioDivisas(double tipoCambio)
        {
            TipoCambio = tipoCambio;
        }

        public double ConvertirADolares()
        {
            double dolares = Colones / TipoCambio;
            return dolares;
        }
    }
}
