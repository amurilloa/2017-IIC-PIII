﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Telefono
    {
        const int COS_X_MIN = 3;
        const int COS_LLA = 5;
        const int LIM_LLA = 3;

        public int Duracion { get; set; }

        public double CalcularCosto()
        {
            return Duracion <= LIM_LLA ? COS_LLA : COS_LLA + ((Duracion - LIM_LLA) * COS_X_MIN);
        }
    }
}
