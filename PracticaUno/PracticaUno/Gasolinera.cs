﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Gasolinera
    {
        private const double GAL_LIT = 3.785;

        public double PrecioLitro { get; set; }

        public Gasolinera(double precio)
        {
            PrecioLitro = precio;
        }

        public double CobroEnLitros(double galones)
        {
            return galones * GAL_LIT * PrecioLitro;
        }
    }
}
