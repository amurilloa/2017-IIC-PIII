﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaUno
{
    class Temperatura
    {
        public double GradosCentigrados { get; set; }

        public double ConvertirAFarenheit()
        {
            return (9 * GradosCentigrados / 5) + 32;
        }
    }
}
