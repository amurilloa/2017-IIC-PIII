﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TallerInterfazAllanMunrillo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void multiplicaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Multiplicacion mul = new Multiplicacion();
            mul.Visible = true;
        }
    }
}
