﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dados
{
    class Logica
    {
        private Random random;

        public Logica()
        {
            random = new Random();
        }

        public int siguiente() {
            return random.Next(0, 6);
        }
    }
}
