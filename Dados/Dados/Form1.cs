﻿using System;///./
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dados
{
    public partial class Form1 : Form
    {
        private Logica log;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            log = new Logica();
            pictureBox1.Image = imagenes.Images[log.siguiente()];
            pictureBox2.Image = imagenes.Images[imagenes.Images.Count-1];
            pictureBox1.Image = imagenes.Images[0];
            pictureBox2.Image = imagenes.Images[log.siguiente()];

        }

        private void button1_Click(object sender, EventArgs e)
        {
            simularLanzamiento();
        }

        private void simularLanzamiento()
        {
            for (int i = 1; i < 20; i++)
            {
                pictureBox1.Image = imagenes.Images[log.siguiente()];
                pictureBox2.Image = imagenes.Images[log.siguiente()];
                Thread.Sleep(i * 15);
                Refresh();
            }
        }
    }
}
