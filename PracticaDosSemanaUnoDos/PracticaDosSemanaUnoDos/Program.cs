﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaDosSemanaUnoDos
{
    class Program
    {

        static int LeerInt(string mensaje)
        {
            ET:
            Console.WriteLine(mensaje);
            string texto = Console.ReadLine();
            int num = 0;
            if (!Int32.TryParse(texto, out num))
            {
                Console.Write("Favor intente nuevamente, ");
                goto ET;
            }
            return num;
        }

        static Boolean EsPrimo(int num) {
            int cont = 0;
            for (int x = 1; x <= num; x++)
            {
                if (num % x == 0)
                {
                    cont++;
                    if (cont > 2)
                    {
                        break;
                    }
                }
            }
            if (cont == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void Main(string[] args)
        {

            goto PR3;
            Console.WriteLine("Digite su nombre: ");
            string nombre = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Hola {0}", nombre);
            Console.WriteLine(DateTime.Now.ToString("G"));

            ET:

            int num1 = LeerInt("Digite el número 1");
            int num2 = LeerInt("Digite el número 2");
            int num3 = LeerInt("Digite el número 3");

            Console.WriteLine("El resultado de {0}{1}{2}={3}", num2, (num1 < 0 ? "+" : "*"), num3, (num1 < 0 ? num2 + num3 : num2 * num3));

            Console.WriteLine("Desea realizarlo nuevamente S/N");
            string res = Console.ReadLine();
            if (res.Equals("S"))
            {
                Console.Clear();
                goto ET;
            }


            int numero = LeerInt("Digite un número para invertir");
            int numeroNuevo = 0;
            while (numero != 0)
            {
                numeroNuevo = numeroNuevo * 10 + (numero % 10);
                numero /= 10;
            }
            Console.WriteLine(numeroNuevo);

            ET1:
            int cantidad = LeerInt("Digite la cantidad de elementos");
            int[] numeros = new int[cantidad];
            int numMax = 0;
            int numMin = 0;
            double media = 0;

            for (int i = 0; i < numeros.Length; i++)
            {
                numeros[i] = LeerInt("Digite el número " + (i + 1) + "/" + cantidad);
                if (i == 0)
                {
                    numMax = numeros[i];
                    numMin = numeros[i];
                    media = numeros[i];
                }
                else
                {
                    //Calculando el máximo
                    if (numMax < numeros[i])
                    {
                        numMax = numeros[i];
                    }
                    //Calculando el mínimo
                    if (numMin > numeros[i])
                    {
                        numMin = numeros[i];
                    }
                    media += numeros[i];
                }

            }
            media /= cantidad;

            Console.WriteLine("El mayor es {0}", numMax);
            Console.WriteLine("El menor es {0}", numMin);
            Console.WriteLine("La media es {0:0.00}", media);

           
            int n1 = LeerInt("Digite el número 1");
            int n2 = LeerInt("Digite el número 2");
            int mcd = 1;
            int inicio = 2;
            while (inicio <= n1 && inicio <= n2)
            {
                if (n1 % inicio == 0 && n2 % inicio == 0)
                {
                    mcd *= inicio;
                    n1 /= inicio;
                    n2 /= inicio;
                    Console.WriteLine(inicio);
                }
                else
                {
                    inicio++;
                }
            }
            Console.WriteLine("El MCD de {0} y {1} es {2}", (n1 * mcd), (n2 * mcd), mcd);
            

            int canConejos = LeerInt("Cantidad de Conejos requeridos");
            int canMeses = LeerInt("Cantidad de meses que espera");

            int canHijos = 0;
            int canPadres = 1;
            int meses = 1;
            Console.WriteLine("{0} P={1} H={2}", meses, canPadres, canHijos);

            while ((canHijos + canPadres) <= canConejos) {
                int temp = canPadres;
                canPadres = canPadres + canHijos;
                canHijos = temp;
                meses++;
                Console.WriteLine("{0} P={1} H={2}",meses, canPadres,canHijos);
            }
            Console.WriteLine("Debo esperar {0} meses para tener {1} parejas de conejos", meses, canConejos);


            canHijos = 0;
            canPadres = 1;
            meses = 1;
            Console.WriteLine("{0} P={1} H={2}", meses, canPadres, canHijos);

            while (meses < canMeses)
            {
                int temp = canPadres;
                canPadres = canPadres + canHijos;
                canHijos = temp;
                meses++;
                Console.WriteLine("{0} P={1} H={2}", meses, canPadres, canHijos);
            }
            Console.WriteLine("En {0} meses voy a tener {1} parejas de conejos",meses, (canPadres+canHijos));

           
            int num = LeerInt("Digite un número");
            int cont = 0;
            for (int x = 1; x <= num; x++)
            {
                if (num % x == 0)
                {
                    cont++;
                    if (cont > 2)
                    {
                        break;
                    }
                }
            }
            if (cont == 2)
            {
                Console.WriteLine("El número es primo");
            }
            else {
                Console.WriteLine("No es primo");
            }
            PR3:
            int con = 0;
            int conP = 0;
            while (conP < 10000) {
                if (EsPrimo(con))
                {
                    conP++;
                   
                }
                con++;
            }
            Console.WriteLine(con);


            Console.ReadKey();

        }
    }
}
