﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DadosAllanMurillo.Entities
{
    public class JugadorE
    {
        public string Nombre { get; set; }
        public int Puntos { get; set; }
        public int Lanzamientos { get; set; }

        public override string ToString() {
            return Nombre + "\t" + Puntos + "/" + Lanzamientos;
        }
    }    
}
