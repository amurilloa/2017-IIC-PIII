﻿using DadosAllanMurillo.BOL;
using DadosAllanMurillo.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DadosAllanMurillo
{
    public partial class DadoFrm : Form
    {
        private JuegoBOL juego;
        public DadoFrm()
        {
            InitializeComponent();
            juego = new JuegoBOL();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            juego.LanzarDados(Convert.ToInt32(numericUpDown1.Value));
            pbDado1.Image = imageList1.Images[juego.DadoUno.Valor - 1];
            pbDado2.Image = imageList1.Images[juego.DadoDos.Valor - 1];
            lblPuntos.Text = juego.Jugador.Puntos.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string datos = textBox1.Text;
            if (datos.Length > 0) {
                button2.Enabled = true;
            } else{
                button2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            juego.Finalizar(textBox1.Text);
            pbDado1.Image = null;
            pbDado2.Image = null;
            lblPuntos.Text = juego.Jugador.Puntos.ToString();
            textBox1.Text = "";
            listBox1.DataSource = null;
            listBox1.DataSource = juego.Jugadores;
        }
    }
}
