﻿using DadosAllanMurillo.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DadosAllanMurillo.BOL
{
    public class JuegoBOL
    {
        public List<JugadorE> Jugadores { get; set; }
        public JugadorE Jugador { get; set; }
        public DadoE DadoUno { get; set; }
        public DadoE DadoDos { get; set; }

        public JuegoBOL()
        {
            Jugador = new JugadorE
            {
                Nombre = "",
                Puntos = 0,
                Lanzamientos = 0
            };
            DadoUno = new DadoE();
            DadoDos = new DadoE();
            Jugadores = new List<JugadorE>();

        }

        public void LanzarDados(int numero)
        {
            Random r = new Random();
            DadoUno.Valor = r.Next(1, 7);
            DadoDos.Valor = r.Next(1, 7);
            Jugador.Lanzamientos++;
            if (numero == DadoUno.Valor + DadoDos.Valor)
            {
                Jugador.Puntos += 3;
            }
            else
            {
                Jugador.Puntos -= 1;
            }

        }

        public void Finalizar(string nombre)
        {
            Jugador.Nombre = nombre;
            Jugadores.Add(Jugador);
            Jugador = new JugadorE
            {
                Nombre = "",
                Puntos = 0,
                Lanzamientos = 0
            };
        }
    }
}
