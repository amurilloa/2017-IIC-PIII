﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProgramacionCapasEntities;
using Npgsql;
using System.Configuration;

namespace ProgramacionCapasDAL
{
    public class PersonaDal
    {
        public Persona ConsultarPorFiltro(string cedula)
        {
            Persona per = null;
            using (NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["conStr"].ConnectionString))
            {
                conn.Open();

                string sql = "select  * from persona where cedula=@cedula";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                Console.WriteLine(cedula);
                cmd.Parameters.AddWithValue("@cedula", cedula);

                NpgsqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    per = CargarPersona(dr);
                }
                return per;
            }
        }

        private Persona CargarPersona(NpgsqlDataReader dr)
        {
            Persona p = new Persona
            {
                Id = (Int32)dr["id"],
                Cedula = dr["cedula"].ToString(),
                Nombre= dr["nombre"].ToString()
            };
            return p;
        }
    }
}
