﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramacionCapasDAL
{
    public class Conexion
    {

        public static NpgsqlConnection ConexionSQL {
            get
            {
                NpgsqlConnection con = new NpgsqlConnection()
                {
                    ConnectionString = "Server=127.0.0.1;" +
                    "User Id=postgres;" +
                    "Password=postgres;" +
                    "Database=progra_db"
                };
                return con;
            }
        }

      
    }
}
