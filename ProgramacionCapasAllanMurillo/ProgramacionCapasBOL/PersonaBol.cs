﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProgramacionCapasEntities;
using ProgramacionCapasDAL;

namespace ProgramacionCapasBOL
{
    public class PersonaBol
    {
        public Persona ConsultarPersona(string cedula)
        {
            PersonaDal pdal = new PersonaDal();
            return pdal.ConsultarPorFiltro(cedula);
        }
    }
}
