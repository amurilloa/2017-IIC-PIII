﻿using ProgramacionCapasBOL;
using ProgramacionCapasEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramacionCapasAllanMurillo
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PersonaBol bol = new PersonaBol();
            Persona p = bol.ConsultarPersona(textBox1.Text.Trim());
            textBox1.Text = p.Nombre;

        }
    }
}
